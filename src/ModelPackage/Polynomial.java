package ModelPackage;

import ExceptionPackage.IllegalCharactersException;
import ExceptionPackage.IllegalExpressionException;
import ExceptionPackage.MultiplePolynomialLettersException;

import java.util.HashMap;
import java.util.regex.*;

public class Polynomial {
    private HashMap<Integer, Monomial> Polynome;
    String variable = "";
    int maxPower;

    Polynomial(String var, int pow) {
        variable = var;
        maxPower = pow;
        Polynome = new HashMap<>();
    }

    public Polynomial(String expression) throws IllegalCharactersException, MultiplePolynomialLettersException, IllegalExpressionException {
        Polynome = new HashMap<>();
        Pattern regxPattern;
        Matcher regxMatcher;
        validateCharacters(expression);
        validateVariable(expression);
        regxPattern = Pattern.compile("[\\w.^]+|[+-]");
        regxMatcher = regxPattern.matcher(expression);
        String sign = "";
        String snip;
        while (regxMatcher.find()) {
            snip = regxMatcher.group().trim();
            if (snip.length() == 1 && !Character.isLetterOrDigit(snip.charAt(0))) { //if snip is a sign
                sign = snip;
                if (regxMatcher.find() && Character.isLetterOrDigit(regxMatcher.group().trim().charAt(0))) {//if monomial exists beyond sign
                    snip = regxMatcher.group().trim();
                } else {
                    throw new IllegalExpressionException(snip, expression.indexOf(snip));
                }
            }
            if(snip.contains(variable) && snip.indexOf('^') == -1 && Character.isDigit(snip.charAt(snip.length() - 1))) {//avoiding situations like X6 instead of X^6
                throw new IllegalExpressionException(snip, expression.indexOf(snip));
            }
            Monomial M = new Monomial(sign + snip, variable);
            maxPower = M.getPower() > maxPower ? M.getPower() : maxPower;
            Monomial N = Polynome.get(M.getPower());
            Polynome.put(M.getPower(), Operation.addMonomials(M, N));
        }
    }

    private void validateCharacters(String expression) throws IllegalCharactersException {
        Pattern pattern = Pattern.compile("[^-+.^ \\w]+|[_C]");
        Matcher matcher = pattern.matcher(expression);

        if(matcher.find()) {
            throw new IllegalCharactersException(matcher.group().trim(), expression.indexOf(matcher.group().trim()));
        }
    }

    private void validateVariable(String expression) throws MultiplePolynomialLettersException {
        Pattern pattern = Pattern.compile("[A-Za-z]+");
        Matcher matcher = pattern.matcher(expression);

        while (matcher.find()) {
            if (variable.isEmpty()) {
                variable = matcher.group().trim();
            } else if (!variable.equals(matcher.group().trim())) {
                throw new MultiplePolynomialLettersException(variable, expression.indexOf(variable), matcher.group().trim(), expression.indexOf(matcher.group().trim()));
            }
        }
        if (variable.isEmpty()) {
            variable = "#UNDEF#";
        }
    }

    public Polynomial copy() {
        Polynomial P = new Polynomial(this.variable, this.maxPower);
        P.Polynome = new HashMap<>();
        for (int i = 0; i <= this.maxPower; i++) {
            if(this.getM(i) != null) {
                P.putM(i, new Monomial(this.getM(i)));
            }
        }
        return P;
    }

    boolean isEmpty() {
        return maxPower < 0;
    }
    boolean isVarUndefined () { return variable.equals("#UNDEF#"); }


    Monomial getM(int powerIndex) {
        return Polynome.get(powerIndex);
    }
    void putM(int powerIndex, Monomial M) {
        Polynome.put(powerIndex, M);
    }
    void updateM(int powerIndex, Monomial M) {
        Monomial prev = Polynome.get(powerIndex);
        Polynome.put(powerIndex, Operation.addMonomials(prev, M));
    }

    void updatePower() {//updates max power
        while(maxPower != 0 && (getM(maxPower) == null || getM(maxPower).getCoefficient() == 0)) {
            maxPower--;
        }
    }

    public String toString() {
        if (maxPower < 0) { return null; }
        String s = "";
        for (int i = maxPower; i >= 0; i--) {
            Monomial M = Polynome.get(i);
            if (M != null) {
                s += M;
            }
        }
        if(s.equals("")) {
            return "0";
        }
        s = s.charAt(0) == '+' ? s.substring(2) : s.substring(0, 1) + s.substring(2);
        return s;
    }
}
