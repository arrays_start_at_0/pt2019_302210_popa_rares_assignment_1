package ModelPackage;

import ExceptionPackage.DifferentPolynomialVariableException;
import ExceptionPackage.DivisionByZeroException;

public class Operation {

    private static void validateVariable(Polynomial P, Polynomial Q) throws DifferentPolynomialVariableException {
        if (P.isVarUndefined()) { P.variable = Q.variable; }
        if (Q.isVarUndefined()) { Q.variable = P.variable; }
        if (!P.variable.equals(Q.variable)) { throw new DifferentPolynomialVariableException(P.variable, Q.variable); }
    }

    static Monomial addMonomials(Monomial M, Monomial N) {
        if (M == null) { return N; }
        if (N == null) { return M; }
        return new Monomial(M.getPower(), M.getCoefficient() + N.getCoefficient(), M.variable);
    }

    private static Monomial multiplyMonomials(Monomial M, Monomial N) {
        if (M == null || N == null) { return null; }
        return new Monomial(M.getPower() + N.getPower(), M.getCoefficient() * N.getCoefficient(), M.variable);
    }

    private static Monomial divideMonomials(Monomial M, Monomial N) throws DivisionByZeroException {
        if (N.getCoefficient() == 0) { throw new DivisionByZeroException(); }
        return new Monomial(M.getPower() - N.getPower(), M.getCoefficient() / N.getCoefficient(), M.variable);
    }

    public static Polynomial derive(Polynomial P) {
        Polynomial R = new Polynomial(P.variable, P.maxPower - 1);
        for (int i = 0; i <= P.maxPower; i++) {
            if (P.getM(i) != null) {
                Monomial M = new Monomial(P.getM(i));
                M.derive();
                if (M.getPower() >= 0) {
                    R.putM(M.getPower(), M);
                }
            }
        }
        if (R.maxPower == 0) { R.variable = "#UNDEF#"; } //if polynomial is a constant, its variable is set to undefined
        return R;
    }

    public static Polynomial integrate(Polynomial P) {
        Polynomial R = new Polynomial(P.variable, P.maxPower + 1);
        for (int i = P.maxPower; i >= 0; i--) {
            if (P.getM(i) != null) {
                Monomial M = new Monomial(P.getM(i));
                M.integrate();
                R.putM(M.getPower(), M);
            }
        }
        R.putM(0, new Monomial(P.variable));//adds a constant C to the back of the polynome
        return R;
    }

    public static Polynomial add(Polynomial P, Polynomial Q) throws DifferentPolynomialVariableException {
        return merge(P, Q);
    }

    public static Polynomial sub(Polynomial P, Polynomial Q) throws DifferentPolynomialVariableException {
        Polynomial R = Q.copy();
        for (int i = R.maxPower; i >= 0; i--) {
            Monomial M = R.getM(i);
            if (M != null) {
                M.scalarProduct(-1);
            }
        }
        return merge(P, R);
    }

    static private Polynomial merge(Polynomial P, Polynomial Q) throws DifferentPolynomialVariableException {
        validateVariable(P, Q);
        int maxPow = P.maxPower > Q.maxPower ? P.maxPower : Q.maxPower;
        Polynomial R = new Polynomial(P.variable, maxPow);
        for (int i = maxPow; i >= 0; i--) {
            Monomial M = addMonomials(P.getM(i), Q.getM(i));
            R.putM(i, M);
        }
        R.updatePower();
        return R;
    }

    public static Polynomial product(Polynomial P, Polynomial Q) throws DifferentPolynomialVariableException {
        validateVariable(P, Q);
        Polynomial R = new Polynomial(P.variable, P.maxPower + Q.maxPower);
        for (int i = 0; i <= P.maxPower; i++) {
            for (int j = 0; j <= Q.maxPower; j++) {
                Monomial product = multiplyMonomials(P.getM(i), Q.getM(j));
                R.updateM(i + j, product);
            }
        }
        return R;
    }

    //N - divident
    //D - divisor
    //Q - quotient
    //R - remainder
    public static Polynomial[] division(Polynomial N, Polynomial D) throws DifferentPolynomialVariableException, DivisionByZeroException {
        validateVariable(N, D);
        Polynomial Q = new Polynomial(N.variable, 0);
        Polynomial R = N.copy();
        do {
            Monomial M = divideMonomials(R.getM(R.maxPower), D.getM(D.maxPower));
            Polynomial temp = new Polynomial(M.variable, M.getPower());
            temp.putM(M.getPower(), M);
            Q = add(Q, temp);
            R = sub(R, product(temp, D));
            Q.updatePower();
        } while (!R.isEmpty() && R.maxPower != 0 && (R.maxPower >= D.maxPower));
        return new Polynomial[]{Q, R};
    }
}
