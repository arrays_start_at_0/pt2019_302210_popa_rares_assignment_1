package ViewPackage;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import java.util.LinkedList;

import ExceptionPackage.*;
import ModelPackage.*;

public class View extends Application {

    private LinkedList<Polynomial> selectedItems;
    private Polynomial Result;
    private Polynomial[] QR = {null, null};

    private TextField inputField;
    private TextField outputField;
    private ListView<Polynomial> listView;
    private ObservableList<Polynomial> pList;

    private Button submitButton;
    private Button saveButton;

    public static void main(String[] args) { launch(args); }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Polynomial Calculator");

        selectedItems = new LinkedList<>();

        inputField = new TextField();
        inputField.setPromptText("X^2 - 2X + 1");
        HBox.setHgrow(inputField, Priority.ALWAYS);

        submitButton = new Button("Submit");
        submitButton.setPrefWidth(80);

        HBox inputLayout = new HBox(10);
            inputLayout.setAlignment(Pos.CENTER);
        inputLayout.getChildren().addAll(inputField, submitButton);
        /*----------------------------------------------------------------------------*/
        pList = FXCollections.observableArrayList();
        listView = new ListView<>(pList);
        listView.fixedCellSizeProperty();
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        listView.setOnMouseClicked(e -> selection());
        /*----------------------------------------------------------------------------*/
        Button addition = new Button("+");
        Button subtraction = new Button("-");
        Button multiplication = new Button("*");
        Button division = new Button("/");
        Button derrivation = new Button("( )'");
        Button integration = new Button("∫");

        HBox.setHgrow(addition, Priority.ALWAYS);
        HBox.setHgrow(subtraction, Priority.ALWAYS);
        HBox.setHgrow(multiplication, Priority.ALWAYS);
        HBox.setHgrow(division, Priority.ALWAYS);
        HBox.setHgrow(derrivation, Priority.ALWAYS);
        HBox.setHgrow(integration, Priority.ALWAYS);

        addition.setMaxWidth(Double.MAX_VALUE);
        subtraction.setMaxWidth(Double.MAX_VALUE);
        multiplication.setMaxWidth(Double.MAX_VALUE);
        division.setMaxWidth(Double.MAX_VALUE);
        derrivation.setMaxWidth(Double.MAX_VALUE);
        integration.setMaxWidth(Double.MAX_VALUE);

        HBox commandLayout = new HBox(10);
        commandLayout.setAlignment(Pos.CENTER);
        commandLayout.getChildren().addAll(addition, subtraction, multiplication, division, derrivation, integration);
        /*----------------------------------------------------------------------------*/
        outputField = new TextField();
        outputField.setEditable(false);
        HBox.setHgrow(outputField, Priority.ALWAYS);

        saveButton = new Button("Save");
        saveButton.setPrefWidth(80);

        HBox outputLayout = new HBox(10);
        outputLayout.setAlignment(Pos.CENTER);
        outputLayout.getChildren().addAll(outputField, saveButton);

        VBox mainLayout = new VBox(20);
        mainLayout.setPadding(new Insets(20, 20, 20, 20));
        mainLayout.getChildren().addAll(inputLayout, listView, commandLayout, outputLayout);

        Scene scene = new Scene(mainLayout, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();

        submitButton.setOnAction(e -> EHBI(submitButton));
        addition.setOnAction(e -> EHBI(addition));
        subtraction.setOnAction(e -> EHBI(subtraction));
        multiplication.setOnAction(e -> EHBI(multiplication));
        division.setOnAction(e -> EHBI(division));
        derrivation.setOnAction(e -> EHBI(derrivation));
        integration.setOnAction(e -> EHBI(integration));
        saveButton.setOnAction(e -> EHBI(saveButton));


        scene.setOnKeyPressed(e -> keyPress(e));
    }

    private void EHBI(Button B) { //event handled button interaction
        try {
            switch (B.getText()) {
                case "Submit":
                    addPolynomial();
                    break;
                case "Save":
                    savePolynomial();
                    break;
                case "+":
                    add();
                    break;
                case "-":
                    sub();
                    break;
                case "*":
                    prod();
                    break;
                case "/":
                    div();
                    break;
                case "( )'":
                    differentiate();
                    break;
                case "∫":
                    integrate();
                    break;
            }
        } catch (IllegalCharactersException e) {
            errorMsg("Illegal chracter used", e.getMessage());
        } catch (MultiplePolynomialLettersException e) {
            errorMsg("Multiple variables used", e.getMessage());
        } catch (IllegalExpressionException e) {
            errorMsg("Illegal expression", e.getMessage());
        } catch (DifferentPolynomialVariableException e) {
            errorMsg("Different variable Polynomials", e.getMessage());
        } catch (DivisionByZeroException e) {
            errorMsg(e.getMessage(), "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void keyPress(KeyEvent E) {
        switch (E.getCode()) {
            case ENTER:
                EHBI(submitButton);
                break;
            case DELETE:
                for (Polynomial P : listView.getSelectionModel().getSelectedItems()) {
                    pList.remove(P);
                }
                listView.getSelectionModel().clearSelection();
                break;
            case S:
                EHBI(saveButton);
                break;
        }
    }

    private void addPolynomial() throws MultiplePolynomialLettersException, IllegalCharactersException, IllegalExpressionException {
        if (inputField.getText().isEmpty()) {
            return;
        }
        pList.add(new Polynomial(inputField.getText()));
        inputField.setText("");
        inputField.requestFocus();
    }

    private void selection() {
        MultipleSelectionModel<Polynomial> selected = listView.getSelectionModel();

        //delete deselected items
        if (selectedItems.contains(selected.getSelectedItem())) {
            selectedItems.remove(selected.getSelectedItem());
            selected.clearSelection(selected.getSelectedIndex());
        }

        //add newly selected items
        for (Polynomial I : selected.getSelectedItems()) {
            if (!selectedItems.contains(I)) {
                selectedItems.addLast(I);
            }
        }
        //remove selected items so that only two are selected at one time
        while (selectedItems.size() > 2) {
            selectedItems.removeFirst();
        }

        //update list view (could be done simpler)
        for (Polynomial I : listView.getItems()) {
            if (selectedItems.contains(I)) {
                selected.select(I);
            } else {
                int index = listView.getItems().indexOf(I);
                selected.clearSelection(index);
            }
        }
    }

    private void savePolynomial() {
        if (QR[0] != null || QR[1] != null) {
            pList.add(QR[0]);
            pList.add(QR[1]);
            outputField.setText("");
            listView.requestFocus();
        } else {
            if (Result != null) {
                pList.add(Result);
                outputField.setText("");
                listView.requestFocus();
            } else {
                inputField.requestFocus();
            }
        }
        QR[0] = null;
        QR[1] = null;
        Result = null;
    }

    private void errorMsg(String header, String description) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("ERROR");
        alert.setHeaderText(header);
        alert.setContentText(description);
        alert.showAndWait();
    }

    private Polynomial[] selectTwoPolynomials() throws Exception {
        Polynomial[] slate = {null, null};
        switch (selectedItems.size()) {
            case 0:
                return null;
            case 1:
                if (inputField.getText().isEmpty()) {
                    errorMsg("Insufficient input", "");
                } else {
                    slate[0] = (new Polynomial(inputField.getText()));
                    slate[1] = (selectedItems.get(0));
                }
                break;
            case 2:
                slate[0] = (selectedItems.get(0));
                slate[1] = (selectedItems.get(1));
                break;
        }
        return slate;
    }

    private Polynomial selectOnePolynomial() throws Exception {//for integration and derivation
        switch (selectedItems.size()) {
            case 0:
                if (!inputField.getText().isEmpty()) {
                    return new Polynomial(inputField.getText());
                }
                break;
            case 1:
                return selectedItems.get(0);
            case 2:
                errorMsg("Too many items selected.", "");
                break;
        }
        return null;
    }

    private void add() throws Exception {
        Polynomial[] slate = selectTwoPolynomials();
        Result = Operation.add(slate[0], slate[1]);
        outputField.setText(Result.toString());
    }

    private void sub() throws Exception {
        Polynomial[] slate = selectTwoPolynomials();
        Result = Operation.sub(slate[0], slate[1]);
        outputField.setText(Result.toString());
    }

    private void prod() throws Exception {
        Polynomial[] slate = selectTwoPolynomials();
        Result = Operation.product(slate[0], slate[1]);
        outputField.setText(Result.toString());
    }

    private void div() throws Exception {
        Polynomial[] slate = selectTwoPolynomials();
        QR = Operation.division(slate[0], slate[1]);
        outputField.setText("Q =  " + QR[0] + " ; R = " + QR[1]);
    }

    private void differentiate() throws Exception {
        Polynomial P = selectOnePolynomial();
        Result = Operation.derive(P);
        outputField.setText(Result.toString());
    }

    private void integrate() throws Exception {
        Polynomial P = selectOnePolynomial();
        Result = Operation.integrate(P);
        outputField.setText(Result.toString());
    }
}
