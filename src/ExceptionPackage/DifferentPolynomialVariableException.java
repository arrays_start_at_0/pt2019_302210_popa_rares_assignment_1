package ExceptionPackage;

public class DifferentPolynomialVariableException extends Exception {
    public DifferentPolynomialVariableException (String var1, String var2) {
        super("The two polynomials are of different variables: " + var1 + " and  " + var2 + ".");
    }
}
