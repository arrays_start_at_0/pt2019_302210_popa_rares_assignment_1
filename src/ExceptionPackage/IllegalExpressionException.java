package ExceptionPackage;

public class IllegalExpressionException extends Exception {
    public IllegalExpressionException(String illegallSnip, int snipIndex) {
        super("Illegal expression in input field: " + illegallSnip + " at position " + snipIndex + ".");
    }
}
