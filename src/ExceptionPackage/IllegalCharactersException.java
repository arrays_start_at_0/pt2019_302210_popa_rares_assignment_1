package ExceptionPackage;

public class IllegalCharactersException extends Exception {
    public IllegalCharactersException(String string, int pos) {
        super("Invalid character(s) \"" + string + "\" at position: " + pos + ".");
    }
}
